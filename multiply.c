#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <math.h>


/*utility functions*/
void repeatChar(char c, int count)
{
	for (int i = count; i-- > 0; putchar(c));
}

void printNumSpecial(int sign, const char* strBase, long exponent)
{
	if (sign < 0)
		putchar('-');

	printf("%s", strBase);						//print base
	repeatChar('0', exponent);				//follow with exponent

	puts("");
}

//itoa is not defined in ANSI-C...DUH
void printLongLong(int sign, unsigned long long value)
{
	//find log10 first
	//If x is negative, it causes a domain error.
	//If x is zero, it may cause a pole error(depending on the library implementation).

	if (value == 0)
	{
		puts("0");
		return;
	}

	if (sign < 0)
		putchar('-');

	size_t exp = log10(value);					//want it floored
	unsigned long long tmpVal = value;			//keep for comparison
		
	for (int pos = exp; pos >=0; --pos)
	{
		size_t max = pow(10, pos);
		
		putchar(tmpVal / max + '0');

		tmpVal %= max;
	}

	puts("");
}


/*BigNum struct & methods

"			+000000000001234567899987000000000000"
			|			|		    ||			|
			|			|		    ||			|
		  sign			<-strScalar-><-exponent->
*/
typedef struct
{
	const char* strScalar;

	int sign, hasOwerflown, isValid;

	unsigned long long absValue, exponent;

	size_t length;						//strScalar length
} BigNum;


/*ctor*/
void BigNum_init(BigNum* num, const char* strNum)
{
	errno = 0;
	char* endptr = 0;
	
	num->strScalar = strNum;

	long long value = strtoll(num->strScalar, &endptr, 10);					//strtol dictates what is a valid base10 number

	num->isValid = *endptr == 0;
	

	if (num->isValid)
	{
		num->hasOwerflown = errno;
		num->sign = value < 0 ? -1 : 1;									//the sign is preserved on owerflow
		num->absValue = value * num->sign;								//will disregard if num->hasOwerflown

		num->strScalar = strpbrk(num->strScalar, "-+0123456789");		//skip all leading non-number junk

		if (!isdigit(*num->strScalar))									//number is valid, but 1st char is not a digit (shall be a sign)
		{
			assert(*num->strScalar == '-' || *num->strScalar == '+');	//this CANT happen!!!

			num->strScalar++;											//advance past the sign
		}

		num->strScalar += strspn(num->strScalar, "0");					//skip leading zeros

		num->length = strlen(num->strScalar);							//the "usable" length
		
		if (num->absValue)												//dont care for zero
		{
			//strip following zeros and put them in exponent
			long long nonZeroLength = num->length;						//TODO: find_last_of("-+123456789")
			for (; nonZeroLength && num->strScalar[nonZeroLength - 1] == '0'; --nonZeroLength);

			num->exponent = num->length - nonZeroLength;
			num->length = nonZeroLength;
		}
	}
}


int BigNum_isZero(const BigNum* num)
{
	return num->isValid && !num->hasOwerflown && num->absValue == 0;
}


int multiply_special_one(const BigNum* numLeft, const BigNum* numRight)
{
	assert(numLeft->isValid && numRight->isValid);


	if (strncmp(numLeft->strScalar, "1", numLeft->length) == 0)					//numLeft is 10^n
	{
		printNumSpecial(numLeft->sign * numRight->sign, numRight->strScalar, numLeft->exponent);			//print numRight followed by numLeft's zeros

#ifdef _DEBUG
		puts("END multiply_special_one");
#endif // _DEBUG

		return 1;
	}

	return 0;
}



//1 - handled, shall exit; 0 - not handled, continue
int multiply_special(const BigNum* numLeft, const BigNum* numRight)
{
	assert(numLeft->isValid && numRight->isValid);
	
	
	if (BigNum_isZero(numLeft) || BigNum_isZero(numRight))
	{
		puts("0");
		return 1;
	}


	if (!numLeft->hasOwerflown && !numRight->hasOwerflown)									//try normal multiplication
	{
		unsigned long long result = numLeft->absValue * numRight->absValue;

		if (result == 0 || result / numLeft->absValue == numRight->absValue)				//no overflow
		{
			printLongLong(numLeft->sign * numRight->sign, result);
			
#ifdef _DEBUG
			puts("END multiply_special");
#endif // _DEBUG
			
			return 1;
		}
	}

	return multiply_special_one(numLeft, numRight) || multiply_special_one(numRight, numLeft);
}


int multiply(const BigNum* numLeft, const BigNum* numRight)
{
	if (!numLeft->isValid || !numRight->isValid)
	{
		fputs("please provide 2 base10 numbers", stderr);
		return -1;
	}

	if (multiply_special(numLeft, numRight))
		return 0;
	

	const long resLen = numLeft->length + numRight->length;
	
	char* arrRes = (char*) calloc(resLen + 1, sizeof(char));		//will waste at most 1 char 
	
#ifdef _DEBUG
	printf("allocated Bytes: %ld\n", resLen + 1);
#endif // _DEBUG

	if (!arrRes)
	{
		fputs("could not allocate memory", stderr);
		return -1;
	}	
	

	//lets do kiddie math
	int prevBonus = 0;
	long resDigitIdx = 0;
	
	for (long num2Idx = numRight->length - 1; num2Idx >= 0; --num2Idx)
	{
		int dig2 = numRight->strScalar[num2Idx] - '0';
		
		prevBonus = 0;
		
		for (long num1Idx = numLeft->length - 1; num1Idx >= 0; --num1Idx)
		{
			int dig1 = numLeft->strScalar[num1Idx] - '0';
						
			resDigitIdx = num2Idx + num1Idx + 1;
			
			int product =  dig1 * dig2 + prevBonus + arrRes[resDigitIdx];
						
			arrRes[resDigitIdx] = product % 10;
			
			prevBonus = product / 10;
		}
		
		if (prevBonus)
			arrRes[--resDigitIdx] = prevBonus;
	}
	
	for (long i = 0; i < resLen; ++i)					//make them printable
		arrRes[i] += '0';
	

	printNumSpecial(numLeft->sign * numRight->sign, arrRes + resDigitIdx, numLeft->exponent + numRight->exponent);		//DO IT!


#ifdef _DEBUG
	printf("memory waste: %ld\n", resDigitIdx);
#endif // _DEBUG


	free(arrRes);								//almost forgot...C++!
	arrRes = 0;	
	
	return 0;
}


int main(int argc, char* argv[])
{
	const char* arg1;
	const char* arg2;

	if (argc < 3)
	{
#ifndef _DEBUG
		fputs("please provide 2 numbers", stderr);
		return -1;
#else
		arg1 = "	9223372036854775807i64";
		arg2 = "-555";
#endif
	}
	else
	{
		arg1 = argv[1];
		arg2 = argv[2];
	}

	/*
	#define LLONG_MAX     9223372036854775807i64       // maximum signed long long int value
	#define LLONG_MIN   (-9223372036854775807i64 - 1)  // minimum signed long long int value
	#define ULLONG_MAX    0xffffffffffffffffui64       // maximum unsigned long long int value
	*/
		
	BigNum numLeft, numRight;

	BigNum_init(&numLeft, arg1);
	BigNum_init(&numRight, arg2);
			
	return multiply(&numLeft, &numRight);
}
