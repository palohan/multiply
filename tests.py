import unittest 
import subprocess 
import os 
import random 

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-d", "--digits", type=int, help="number of digits in a number", default=5000)
parser.add_argument("-s", "--sets", type=int, help="number of sets", default=20)
parser.add_argument("-p", "--program", help="program to run", default='./multiply')
testargs = parser.parse_args()

os.chdir(os.getcwd()) 
random.seed() 

BRUTAL_SIZE=100000

class MyTests(unittest.TestCase):
	def setUp(self):
		pass		
		
	def _testonce(self, num1, num2):
		proc = subprocess.run([testargs.program, str(num1), str(num2)], stdout=subprocess.PIPE)
		proc.check_returncode()		
		
		result = proc.stdout.decode('ascii')
		
		expected = int(num1) * int(num2)
		self.assertEqual(int(result), expected, "Expected: {}, Got: {}".format(expected, result))
		
	def test_generic(self, arg_size=testargs.digits, set_size=testargs.sets):
		for i in range(set_size):
			num1 = random.getrandbits(arg_size) * random.choice([-1, 1])
			num2 = random.getrandbits(arg_size) * random.choice([-1, 1])
			
			self._testonce(num1, num2)
						
	def test_brutal(self):
		self.test_generic(BRUTAL_SIZE, 1)
				
	def test_manual(self):
		with open('test_manual.txt') as testfile:		
			for line in testfile:
				args = line.strip().split()
				if len(args):
					self._testonce(*args) 

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(MyTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
    
    
#getconf ARG_MAX			Note: doesnt work, too big

#xargs --show-limits
#Your environment variables take up 3516 bytes
#POSIX upper limit on argument length (this system): 2091588
#POSIX smallest allowable upper limit on argument length (all systems): 4096
#Maximum length of command we could actually use: 2088072
#Size of command buffer we are actually using: 131072
#Maximum parallelism (--max-procs must be no greater): 2147483647
