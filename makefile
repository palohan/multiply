CFLAGS= -std=c11 -Wall -Wextra -Werror -g 

all: multiply

multiply: multiply.c
	gcc ${CFLAGS} $^ -lm -o multiply
	
multiply_dbg: multiply.c
	gcc ${CFLAGS} -D _DEBUG $^ -lm -o multiply
	
test: multiply
	python3 tests.py
		
clean:
	rm -f multiply
	rm -f multiply_dbg
