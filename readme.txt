Building:
	make multiply
	
	
Cleaning:
	make clean
	
	
Testing:
	python tests.py	

3 sets of tests will run:
	1. generic - 20 sets of 500 digit numbers
	2. brutal - 1 set of 100000 digit numbers
	3. manual - numbers taken from test_manual.txt
